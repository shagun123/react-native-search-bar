import React from 'react';
import {View} from 'react-native';
import SearchBar from './src/components/searchBar';
import VideoList from './src/components/videoList';

class App extends React.Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            searching: false,
            search: '',
            videos: [{
                index: 1,
                title: 'learn Coding',
                description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into el',
            },
                {
                    index: 2,
                    title: 'learn abc',
                    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into el',
                },
                {
                    index: 3,
                    title: 'video 1',
                    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into el',
                },
                {
                    index: 4,
                    title: 'Video 4',
                    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into el',
                }],
            filteredVideos: [{
                index: 1,
                title: 'learn Coding',
                description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into el',
            },
                {
                    index: 2,
                    title: 'learn abc',
                    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into el',
                },
                {
                    index: 3,
                    title: 'video 1',
                    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into el',
                },
                {
                    index: 4,
                    title: 'Video 4',
                    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into el',
                }],
        };
    }

    // initial -> filtered , videos -> equal
    // search -> filtered videos -> searched Videos
    onClear = () => {
        this.setState({
            searching: false,
            search: '',
            filteredVideos: this.state.videos,
        });
    };

    onSearch = (value) => {
        if (value === '') {
            return this.setState({
                search: '',
                searching: false,
                filteredVideos: this.state.videos,
            });
        }
        this.setState({
            searching: true,
            search: value,
        });
        setTimeout(() => {
            const searchedVideos = this.state.videos.filter((video) => video.title.toLowerCase().indexOf(value.toLowerCase()) !== -1);
            this.setState({
                searching: false,
                filteredVideos: searchedVideos,
            });
        }, 400);
    };

    render() {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'stretch',
            }}>
                <View style={{height: 20}}/>
                <SearchBar onChangeText={this.onSearch} value={this.state.search} editing={this.state.search !== ''}
                           loading={this.state.searching} onClear={this.onClear}/>
                <View style={{height: 20}}/>
                <VideoList data={this.state.filteredVideos}/>
            </View>
        );
    }
}

export default App;
